from django.contrib import admin
from .models import SampleCodeModel
from django_sb_codefield.admin import CodeFieldAdminMixin


# Register your models here.
@admin.register(SampleCodeModel)
class SampleCodeModelAdmin(CodeFieldAdminMixin, admin.ModelAdmin):
    list_display = ("name", "created_at", "updated_at")
