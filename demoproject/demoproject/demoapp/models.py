from django.db import models
from django_sb_codefield.fields import CodeField


# Create your models here.
class SampleCodeModel(models.Model):
    name = models.CharField(max_length=200)
    code = CodeField(
        modes=["javascript"],
        options={
            "mode": "javascript",
            "theme": "monokai",
        },
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
