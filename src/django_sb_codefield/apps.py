from django.apps import AppConfig


class DjangoSbCodefieldConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "django_sb_codefield"
