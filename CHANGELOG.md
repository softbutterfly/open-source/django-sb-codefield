# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2024-09-02

* Update dependencies
* Add support for django 5.x.y

## [0.3.0] - 2023-04-29

- Update download and issue tracker links in pyporject.toml

## [0.2.0] - 2023-04-29

- Update package description in README.md and pyproject.toml

## [0.1.0] - 2023-04-29

- Compatible with django admin
- Use codemirror2 widget directly in django using CodeField
